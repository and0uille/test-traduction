.. Test traduction documentation master file, created by
   sphinx-quickstart on Mon May  8 12:41:04 2023.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Test traduction's documentation!
===========================================

.. toctree::
   :maxdepth: 1
   :caption: Contents:

   test1
   test2


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
