��          �               �      �      �        f     	   �     �  4   �     �     �     �  P   �  +   H  �  t     N     ^     n  `   |  	   �     �     �                &  B   :  3   }   :ref:`genindex` :ref:`modindex` :ref:`search` Ceci est le test numéro 1 ! Voyons ce que ça donne. Le but est de tester la traduction avec Weblate. Contents: Indices and tables Pourvu qu'il y ait beaucoup de monde qui contribue ! Test 1 Test2 Weblate, c'est super. Weblate, ce n'est pas facile à configurer. J'espere que ça marchera longtemps. Welcome to Test traduction's documentation! Project-Id-Version: Test traduction
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2023-05-08 13:03+0200
PO-Revision-Date: 2023-05-24 18:39+0000
Last-Translator: Weblate Admin <admin@example.com>
Language: en
Language-Team: English <https://translate.zen-roller-derby.xyz/projects/zen-of-reffing/test-de-traduction/en/>
Plural-Forms: nplurals=2; plural=n != 1;
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
Generated-By: Babel 2.12.1
 :ref:`genindex` :ref:`modindex` :ref:`search` This is the first test! Let's see how it goes. The goal is to test the translation with Weblate. Contents: Indices and tables I hope many people contribute! Test 1 Test2 Weblate is awesome! Weblate is not easy to configure. I hope it works for a long time. Bienvenue sur la documentation de Test traduction ! 